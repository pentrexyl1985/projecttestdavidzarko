package main;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import domen.Role;
import domen.User;
import helper.Helper;
import menu.MenuText;
import services.ServiceLogin;
import services.ServiceLoginImpl;
import services.ServiceUser;
import services.ServiceUserImpl;

public class Main {

	private final Helper helper;
	private final ServiceLogin serviceLogin;
	private final ServiceUser serviceUser;

	public Main() {
		helper = new Helper();
		serviceLogin = new ServiceLoginImpl();
		serviceUser = new ServiceUserImpl();

	}

	public static void main(String[] args) {

		Main main = new Main();
		main.start();

	}

	private void start() {
		Scanner sc;
		int numberOfTrys = 3;
		while (numberOfTrys > 0) {
			try {
				MenuText.showMenu();
				sc = new Scanner(System.in);
				int number = helper.validateInput(sc);
				switch (number) {
				case 1:
					User user = serviceLogin.login();
					if (user == null) {
						numberOfTrys--;
					} else if (user.getRoleName() == Role.ADMIN) {
						numberOfTrys = 0;
						administrationOfUser();
					}
					break;
				case 2:
					User newUser = serviceLogin.singUp();
					break;
				default:
					numberOfTrys--;
					System.err.println("That`s not a option.");
					break;
				}
			} catch (InputMismatchException e) {
				System.err.println(e.getMessage());
				numberOfTrys--;
				if (numberOfTrys == 0)
					System.err.println("Thanks for using our application.");
			}
		}

	}

	private void administrationOfUser() {
		int numberOfTrys = 3;
		Scanner sc;
		while (numberOfTrys > 0) {
			try {
				MenuText.showAdministrationMenu();
				sc = new Scanner(System.in);
				int number = helper.validateInput(sc);
				switch (number) {
				case 1:
					// servise user for add new user
					break;
				case 2:
					List<User> foundList = serviceUser.findAll();
					foundList.stream().forEach(user-> System.out.println(user.toString()));
					break;
				case 3:
					User foundUser = serviceUser.findById();
					System.out.println(foundUser.toString());
					break;
				case 4:
					// update user
					break;
				case 5:
					serviceUser.deleteUser();
					break;
				case 6:
					numberOfTrys = 0;
					System.err.println("Good bye.Thank you for using our application.");
					break;
				default:
					numberOfTrys--;
					System.err.println("That`s not a option.");
					break;
				}
			} catch (InputMismatchException e) {
				System.err.println(e.getMessage());
				numberOfTrys--;
				if (numberOfTrys == 0) {
					System.err.println("U reach max number of trys. Login again.");
					start();
				}
			}
		}

	}

}
