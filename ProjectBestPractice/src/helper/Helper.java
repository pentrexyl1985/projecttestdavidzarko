package helper;

import java.util.InputMismatchException;
import java.util.Scanner;

import domen.Credentials;

public class Helper {

	public int validateInput(Scanner sc) {
		try {
			int number = sc.nextInt();
			return number;
		} catch (Exception e) {
			throw new InputMismatchException("Input isn`t number.");
		}
	}
	
	public String validateEmptyString(Scanner sc) {
		String string = sc.nextLine();
        if(string.equals("")){
           throw new InputMismatchException("Enter something.");
        }else {
        	return string;
        }
        
    }

	public Credentials createCredentials(Scanner sc) {
		System.out.println("Enter username.");
		String username = validateEmptyString(sc);
		System.out.println("Enter password.");
		String password = validateEmptyString(sc);
		Credentials credentials = new Credentials(username, password);
		return credentials;
	}

}
