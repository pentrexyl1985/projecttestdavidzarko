package domen;

import java.io.Serializable;

import enumi.EmisijaTip;

public class Emisija implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int idEmisija;
	private String naziv;
	private String opis;
	private EmisijaTip tip;
	public Emisija(int idEmisija, String naziv, String opis, EmisijaTip tip) {
		super();
		this.idEmisija = idEmisija;
		this.naziv = naziv;
		this.opis = opis;
		this.tip = tip;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public EmisijaTip getTip() {
		return tip;
	}
	public void setTip(EmisijaTip tip) {
		this.tip = tip;
	}
	public int getIdEmisija() {
		return idEmisija;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idEmisija;
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((tip == null) ? 0 : tip.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Emisija)) {
			return false;
		}
		
		Emisija e = (Emisija) obj;
		return Integer.compare(idEmisija, e.getIdEmisija())==0 && naziv.equalsIgnoreCase(e.getNaziv());
		
	}
	@Override
	public String toString() {
		return "Emisija [idEmisija=" + idEmisija + ", naziv=" + naziv + ", opis=" + opis + ", tip=" + tip + "]";
	}
	
	
	

	

}
