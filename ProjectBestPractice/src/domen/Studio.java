package domen;

import java.io.Serializable;

public class Studio implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int idStudio;
	private String naziv;
	private String opis;
	private String lokacija;
	public Studio(int idStudio, String naziv, String opis, String lokacija) {
		super();
		this.idStudio = idStudio;
		this.naziv = naziv;
		this.opis = opis;
		this.lokacija = lokacija;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getLokacija() {
		return lokacija;
	}
	public void setLokacija(String lokacija) {
		this.lokacija = lokacija;
	}
	public int getIdStudio() {
		return idStudio;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idStudio;
		result = prime * result + ((lokacija == null) ? 0 : lokacija.hashCode());
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Studio)) {
			return false;
	}
		Studio s = (Studio) obj;
		return Integer.compare(idStudio, s.getIdStudio())==0 && naziv.equalsIgnoreCase(s.getNaziv());
		
	}
	@Override
	public String toString() {
		return "Studio [idStudio=" + idStudio + ", naziv=" + naziv + ", opis=" + opis + ", lokacija=" + lokacija + "]";
	}
	
	
	
	
	

	

}
