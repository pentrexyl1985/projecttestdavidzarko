package domen;

import java.io.Serializable;
import java.util.Date;

import enumi.OsnovnoSredstvoStatus;
import enumi.OsnovnoSredstvoTip;

public class OsnovnoSredstvo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int idOsnovnoSredstvo;
	private int inventarskiBroj;
	private String naziv;
	private String opis;
	private OsnovnoSredstvoTip tip;
	private Date datumNabavke;
	private OsnovnoSredstvoStatus status;
	
	
	
	


	public OsnovnoSredstvo(int idOsnovnoSredstvo, int inventarskiBroj, String naziv, String opis,
			OsnovnoSredstvoTip tip, Date datumNabavke, OsnovnoSredstvoStatus status) {
		super();
		this.idOsnovnoSredstvo = idOsnovnoSredstvo;
		this.inventarskiBroj = inventarskiBroj;
		this.naziv = naziv;
		this.opis = opis;
		this.tip = tip;
		this.datumNabavke = datumNabavke;
		this.status = status;
	}


	public int getInventarskiBroj() {
		return inventarskiBroj;
	}


	public void setInventarskiBroj(int inventarskiBroj) {
		this.inventarskiBroj = inventarskiBroj;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getOpis() {
		return opis;
	}


	public void setOpis(String opis) {
		this.opis = opis;
	}


	public OsnovnoSredstvoTip getTip() {
		return tip;
	}


	public void setTip(OsnovnoSredstvoTip tip) {
		this.tip = tip;
	}


	public Date getDatumNabavke() {
		return datumNabavke;
	}


	public void setDatumNabavke(Date datumNabavke) {
		this.datumNabavke = datumNabavke;
	}


	public OsnovnoSredstvoStatus getStatus() {
		return status;
	}


	public void setStatus(OsnovnoSredstvoStatus status) {
		this.status = status;
	}


	public int getIdOsnovnoSredstvo() {
		return idOsnovnoSredstvo;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datumNabavke == null) ? 0 : datumNabavke.hashCode());
		result = prime * result + idOsnovnoSredstvo;
		result = prime * result + inventarskiBroj;
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tip == null) ? 0 : tip.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof OsnovnoSredstvo)) {
			return false;
		}
		OsnovnoSredstvo os = (OsnovnoSredstvo) obj;
		return Integer.compare(idOsnovnoSredstvo, os.getIdOsnovnoSredstvo())==0 && Integer.compare(inventarskiBroj, os.getInventarskiBroj())==0;
	}


	@Override
	public String toString() {
		return "OsnovnoSredstvo [idOsnovnoSredstvo=" + idOsnovnoSredstvo + ", inventarskiBroj=" + inventarskiBroj
				+ ", naziv=" + naziv + ", opis=" + opis + ", tip=" + tip + ", datumNabavke=" + datumNabavke
				+ ", status=" + status + "]";
	}
	
	
	
	
	
	
	
	

}


