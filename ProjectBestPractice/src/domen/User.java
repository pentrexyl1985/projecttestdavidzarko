package domen;

import java.io.Serializable;

public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idUser;
	private String name;
	private String surname;
	private String username;
	private String password;
	private Role roleName;
	
	public User() {
		
	}
	public User(int idUser, String name, String surname, String username, String password, Role roleName) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.password = password;
		this.roleName = roleName;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Role getRoleName() {
		return roleName;
	}
	public void setRoleName(Role roleName) {
		this.roleName = roleName;
	}
	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", name=" + name + ", surname=" + surname + ", username=" + username
				+ ", password=" + password + ", roleName=" + roleName + "]";
	}
	
	

}
