package dao;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import domen.Emisija;

public class DaoEmisija {
	private final List<Emisija> listaEmisija = new ArrayList<Emisija>();
	private static final DaoEmisija INSTANCE = new DaoEmisija();

	private DaoEmisija() {
		napuniListu();
	}

	private void napuniListu() {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("Emisija.dat"))) {

			try {
				while (true) {
					listaEmisija.add((Emisija) in.readObject());
				}

			} catch (EOFException e) {
				System.out.println("kraj datoteke");

			} catch (IOException e) {

				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public List<Emisija> getListaEmisija() {
		return listaEmisija;
	}

	public static DaoEmisija getInstance() {
		return INSTANCE;
	}

	public void zapamtiPodatke() {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("Emisija.dat"))){
			for (Emisija emisija : listaEmisija) {
				out.writeObject(emisija);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
