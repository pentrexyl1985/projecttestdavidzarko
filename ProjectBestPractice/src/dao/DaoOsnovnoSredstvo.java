package dao;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import domen.OsnovnoSredstvo;

public class DaoOsnovnoSredstvo {
	private final List<OsnovnoSredstvo> listaOsnovnoSredstvo = new ArrayList<OsnovnoSredstvo>();
	private static final DaoOsnovnoSredstvo INSTANCE = new DaoOsnovnoSredstvo();

	private DaoOsnovnoSredstvo() {
		napuniListu();
	}

	private void napuniListu() {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("OsnovnoSredstvo.dat"))) {

			try {
				while (true) {
					listaOsnovnoSredstvo.add((OsnovnoSredstvo) in.readObject());
				}

			} catch (EOFException e) {
				System.out.println("kraj datoteke");

			} catch (IOException e) {

				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public List<OsnovnoSredstvo> getListaOsnovnoSredstvo() {
		return listaOsnovnoSredstvo;
	}

	public static DaoOsnovnoSredstvo getInstance() {
		return INSTANCE;
	}

	public void zapamtiPodatke() {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("OsnovnoSredstvo.dat"))){
			for (OsnovnoSredstvo osnovnoSredstvo : listaOsnovnoSredstvo) {
				out.writeObject(osnovnoSredstvo);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	

}
