package repository;

import java.util.List;

public interface Repository<T> {
	T save(T t);
	List<T> findAll();
	T findById(Integer id);
	T update(T t);
	void remove (T t);
}
