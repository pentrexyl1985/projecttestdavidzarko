package repository;

import java.util.List;
import java.util.NoSuchElementException;
import dao.DaoUser;
import domen.Credentials;
import domen.User;
import helper.Helper;

public class RepositoryUser implements Repository<User> {
	private final Helper helper;
	private final DaoUser daoUser;

	public RepositoryUser() {
		helper = new Helper();
		daoUser = DaoUser.getInstance();
	}

	public User getByCredentials(Credentials cred) {
		User foundUser = daoUser.getListaUsera().stream().filter(
				user -> cred.getPassword().equals(user.getPassword()) && cred.getUsername().equals(user.getUsername()))
				.findFirst().orElse(null);
		if (foundUser == null) {
			throw new NoSuchElementException();
		} else {
			return foundUser;
		}
	}

	@Override
	public User save(User user) {
		return null;
	}

	@Override
	public List<User> findAll() {
		List<User> list = daoUser.getListaUsera();
		return list;
	}

	@Override
	public User findById(Integer id) {
		User foundUser = daoUser.getListaUsera().stream().filter(user -> user.getIdUser() == id).findFirst().orElse(null);
		if (foundUser == null) {
			throw new NoSuchElementException("No such element found.");
		} else {
			return foundUser;
		}
	}

	@Override
	public User update(User t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(User t) {
		daoUser.getListaUsera().remove(t);
		System.out.println("You deleted user from database.");
		daoUser.zapamtiPodatke();

	}

}
