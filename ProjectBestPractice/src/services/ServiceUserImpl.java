package services;

import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import domen.User;
import helper.Helper;
import repository.RepositoryUser;

public class ServiceUserImpl implements ServiceUser {
	private final RepositoryUser repositoryUser;
	private final Helper helper;

	public ServiceUserImpl() {
		repositoryUser = new RepositoryUser();
		helper = new Helper();
	}

	@Override
	public User findById() {
		int numberOfTrys = 4;
		Scanner sc;
		while (numberOfTrys > 0) {
			try {
				System.out.println("Enter user Id.");
				sc = new Scanner(System.in);
				int id = helper.validateInput(sc);
				try {
					User user = repositoryUser.findById(id);
					return user;
				} catch (NoSuchElementException e) {
					numberOfTrys--;
					System.err.println("No such element found.");
					System.err.println("U have" + " " + numberOfTrys + " " + "more try.");
				}
			} catch (InputMismatchException e) {
				numberOfTrys--;
				System.err.println(e.getMessage());
				if (numberOfTrys == 0)
					System.out.println("Thanks for using our application.");
			}
		}
		return null;
	}

	@Override
	public void deleteUser() {
		User userToDelete = findById();
		repositoryUser.remove(userToDelete);
		
	}

	@Override
	public List<User> findAll() {
		List<User> list = repositoryUser.findAll();
		return list;
	}

}
