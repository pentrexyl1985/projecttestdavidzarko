package services;

import java.util.List;

import domen.User;

public interface ServiceUser {
	User findById();
	void deleteUser();
	List<User> findAll();
}
