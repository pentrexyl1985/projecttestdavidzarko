package services;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import domen.Credentials;
import domen.User;
import helper.Helper;
import repository.RepositoryUser;

public class ServiceLoginImpl implements ServiceLogin {

	private final Helper helper;
	private final RepositoryUser repositoryUser;

	public ServiceLoginImpl() {
		helper = new Helper();
		repositoryUser = new RepositoryUser();
	}

	@Override
	public User login() {
		int numberOfTrys = 4;
		Scanner sc;
		while (numberOfTrys > 0) {
			try {
				sc = new Scanner(System.in);
				Credentials credentials = helper.createCredentials(sc);
				try {
					User user = repositoryUser.getByCredentials(credentials);
					return user;
				} catch (NoSuchElementException e) {
					numberOfTrys--;
					System.err.println("Bad credentials.");
				}
			} catch (InputMismatchException e) {
				numberOfTrys--;
				System.err.println(e.getMessage());
				if (numberOfTrys == 0)
					System.out.println("Thanks for using our application.");
			}
		}
		return null;
	}

	@Override
	public User singUp() {
		// TODO Auto-generated method stub
		return null;
	}

}
